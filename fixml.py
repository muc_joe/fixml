#!/bin/env python3

# Fixes Launchbox XMLs crap in Platforms and AdditionalApplication

import sys, os
import xmltodict

allowedsuffix = ['m3u', 'gdi', 'cdi', 'cue']
game_index = 0
application_index = 0
game_nonm3u_index = 0
has_m3u = []
games = []
removed = []
removed_non_m3u = []
removed_index = []
removed_application_index = []
removed_non_m3u_index = []

if not len(sys.argv) == 2:
  print("No source filename provided.")
  exit()

if not os.path.isfile(sys.argv[1]):
  print("File '" + sys.argv[1] + "' not found.")
  exit()

os.rename(sys.argv[1], sys.argv[1] + ".bak")

completexml = xmltodict.parse(open(sys.argv[1] + ".bak",'r',errors='ignore').read())
print('Read success: ' + sys.argv[1])

def getsuffix(applicationpath):
  gamepath = str(applicationpath)
  offset = gamepath.rfind('.')
  lenth = len(gamepath)
  suffix = gamepath[-(lenth - offset - 1):]
  return suffix

print("Running first pass, removing game entries other than unwanted suffixes: " + str(allowedsuffix))
for game in completexml['LaunchBox']['Game']:
  print("Checking " + game['Title'] + " ...")

  suffix = getsuffix(game['ApplicationPath'])
  if suffix == 'm3u':
    has_m3u.append(game['Title'])
  print("  [ID] " + game['ID'])

  if suffix in allowedsuffix:
    print("  [OK] " + suffix)
    games.append(game['ID'])
  else:
    print("  [REMOVE] " + suffix)
    removed.append(game['ID'])
    removed_index.append(game_index)
  game_index += 1

# If we do not delete outside of the loop and reverse the order, we mess up the xml order for logical reasons.
removed_index.reverse()
for game in removed_index:
  del(completexml['LaunchBox']['Game'][game])

print("Running second pass, removing additional game entries for those that have a playlist... ")
for game in completexml['LaunchBox']['Game']:
  suffix = getsuffix(game['ApplicationPath'])

  if (suffix != 'm3u') and (game['Title'] in has_m3u):
    print("  [REMOVE] " + game['Title'] + " additional entry, has m3u.")
    removed.append(game['ID'])
    removed_non_m3u.append(game['ID'])
    removed_non_m3u_index.append(game_nonm3u_index)
  game_nonm3u_index += 1

removed_non_m3u_index.reverse()
for game in removed_non_m3u_index:
  del(completexml['LaunchBox']['Game'][game])

print("Removing AdditionalApplication entries for deleted or non existing game entries...")
for application in completexml['LaunchBox']['AdditionalApplication']:
  GameID = completexml['LaunchBox']['AdditionalApplication'][application_index]['GameID']
  if (GameID in removed) or (GameID not in games):
    print("  [REMOVE] " +  completexml['LaunchBox']['AdditionalApplication'][application_index]['Id'])
    removed_application_index.append(application_index)
  application_index += 1

# If we do not delete outside of the loop and reverse the order, we mess up the xml order for logical reasons.
removed_application_index.reverse()
for application in removed_application_index:
  del(completexml['LaunchBox']['AdditionalApplication'][application])

with open(sys.argv[1], 'w', newline='\r\n') as result_file:
  result_file.write(xmltodict.unparse(completexml, pretty = True, indent="  "))

#print("games: " + str(len(games)))
#print(games)
#print("has_m3u: " + str(len(has_m3u)))
#print(has_m3u)
#print("removed: " + str(len(removed)))
#print(removed) 
#print("removed_non_m3u: " + str(len(removed_non_m3u)))
#print(removed_non_m3u) 
print("Removed " + str(len(removed) + len(removed_non_m3u)) + " game entries and " + str(len(removed_application_index)) + " AdditionalApplication entries.")
